﻿using UnityEngine;
using System.Collections;

public class SpectrumLogTransform : DataBus {

	public Analyzer source;

	public float logBase = 2;

	float[] outData;

	public override void SetupWithWidth(int width) {

		var logWidth = Mathf.Log(width,logBase);
		int logWidthInt = Mathf.CeilToInt(logWidth);

		outData = new float[logWidthInt];

		ForwardWidthToAllReceivers(logWidthInt);
	}

	public override void Push(float[] data)
	{
		var sourceWidth = data.Length;

		float accumulator = 0f;
		int samples = 0;
		int previousBucket = 0;
		int emptySkip = 0;
		for (int i=0; i < sourceWidth; i++)
		{
			int bucket = Mathf.RoundToInt(Mathf.Log(i+1,logBase)) - emptySkip;
			var power = data[i];
			if (bucket != previousBucket) {
				if (bucket - previousBucket > 1) {
					emptySkip += (bucket - previousBucket - 1);
					bucket = previousBucket + 1;
				}

				if (samples > 0)
				{
					outData[previousBucket] =  accumulator / (bucket+1);
				}
				// reset
				accumulator = 0f;
				samples = 0;
				previousBucket = bucket;
			}
			accumulator += power;
			samples++;
		}

		ForwardToAllReceivers(outData);
	}
}
