﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Mike : MonoBehaviour {

	public AudioSource src;

	public string[] allDevices = new string[0];
	public int deviceIndex;

	void Start () {
		var devices = Microphone.devices;
		allDevices = devices;

		StartCoroutine(Pl());

	}

	IEnumerator Pl()
	{
		yield return new WaitForSeconds(1f);
		var dev = allDevices[deviceIndex];
		
		var clip = Microphone.Start(dev,true,1,48000);
		src.clip = clip;
		yield return new WaitForSeconds(0.2f);

		src.Play();
	}
	
	// Update is called once per frame
	void Update () {
	}
}
