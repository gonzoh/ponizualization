﻿using UnityEngine;
using System.Collections;

public class BounceScale : DataBus {

	public float scaling = 1f;

	public override void Push(float[] data)
	{
		transform.localScale = Vector3.one * (1f + data[0] * scaling);
	}
}
