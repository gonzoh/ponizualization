﻿using UnityEngine;
using System.Collections;

public class Analyzer : MonoBehaviour {

	public AudioSource src;

	[Range(0,8192)]
	public int scopeWidth;

	[Range(0,8192)]
	public int spectrumWidth;

	float[] _scopeData;
	float[] _spectrumData;
	float[] _level;

	public DataBus[] scopeDataBus;
	public DataBus[] spectrumDataBus;
	public DataBus[] levelDataBus;

	public float[] scopeData {
		get {
			return _scopeData;
		}
	}

	public float[] spectrumData {
		get {
			return _spectrumData;
		}
	}

	public float rmsLevel {
		get {
			return _level[0];
		}
	}

	void Start()
	{
		_scopeData = new float[scopeWidth];
		_spectrumData = new float[spectrumWidth];
		_level = new float[1];

		foreach (var db in scopeDataBus) {
			db.SetupWithWidth(scopeWidth);
		}
		foreach (var db in spectrumDataBus) {
			db.SetupWithWidth(spectrumWidth);
		}
		foreach (var db in levelDataBus) {
			db.SetupWithWidth(1);
		}
	}

	void Update () {
		src.GetOutputData(_scopeData,0);

		if (spectrumWidth > 0)
		{
			src.GetSpectrumData(_spectrumData,0,FFTWindow.Hamming);
		}

		// calculate level as RMS
		float total = 1f;
		for (int i=0; i< _spectrumData.Length;i++)
		{
			total+=_spectrumData[i]*_spectrumData[i];
		}
		total = Mathf.Log(total);
		_level[0] = total;

		foreach (var db in scopeDataBus) {
			db.Push(_scopeData);
		}
		foreach (var db in spectrumDataBus) {
			db.Push(_spectrumData);
		}
		foreach (var db in levelDataBus) {
			db.Push(_level);
		}
	}
}
