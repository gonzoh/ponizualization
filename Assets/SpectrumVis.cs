﻿using UnityEngine;
using System.Collections;

public class SpectrumVis : DataBus {

	public float gain = 1f;

	Vector2 scaling;

	public GameObject barPrefab;

	LineRenderer[] lines;
	float[] chasingLevels;

	float _width;

	Vector3 EvaluatePowerAtIndex(float power, int index)
	{
		return (Vector3.up * power * scaling.y);
	}

	public override void SetupWithWidth (int width) {
		_width = width;

		Vector3[] fourCorners = new Vector3[4];
		GetComponent<RectTransform>().GetLocalCorners(fourCorners);
	
		lines = new LineRenderer[width];
		chasingLevels = new float[width];

		for (int i = 0; i<width; i++)
		{
			var go = GameObject.Instantiate(barPrefab) as GameObject;
			go.transform.SetParent(transform);
			go.transform.localPosition = Vector3.Lerp(fourCorners[0],fourCorners[3],i/_width);
			scaling.y = Mathf.Abs (fourCorners[1].y - fourCorners[0].y);
			var lineRenderer = go.GetComponent<LineRenderer>();
			lineRenderer.SetVertexCount(2);
			lineRenderer.SetPosition(0,EvaluatePowerAtIndex(0f,i));
			lineRenderer.useWorldSpace = false;
			lines[i] = lineRenderer;
		}

		Destroy(barPrefab);
	}

	public override void Push(float[] data)
	{
		for (int i =0; i<data.Length; i++)
		{
			var power = Mathf.Atan(Mathf.Log (data[i]+1f,10f)*gain);
			var lineRenderer = lines[i];
			var chasingLevel = chasingLevels[i];
			if (power > chasingLevel)
			{
				chasingLevel = power;
			} else {
				chasingLevel = Mathf.Lerp(chasingLevel,power,Time.deltaTime * 10f);
			}
			chasingLevels[i] = chasingLevel;

			lineRenderer.SetPosition(1,EvaluatePowerAtIndex(chasingLevel,i));
		}
	}
}
