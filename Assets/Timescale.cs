﻿using UnityEngine;
using System.Collections;

public class Timescale : DataBus {

	public float scale;

	public override void Push (float[] data) {
		Time.timeScale = data[0]*scale + 1f;
	}
}
