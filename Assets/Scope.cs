﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]
public class Scope : DataBus {

	LineRenderer lineRenderer;

	public Vector2 scaling;

	public override void SetupWithWidth (int width) {
		lineRenderer = GetComponent<LineRenderer>();
		var scopeWidth = width;
		lineRenderer.SetVertexCount(scopeWidth);
	}
	
	public override void Push (float[] data) {
		for(var i=0;i<data.Length;i++)
		{
			var power = data[i];
			var x = ((float)i / data.Length)-0.5f;
			lineRenderer.SetPosition(i,Vector3.right *x*scaling.x + Vector3.up * power * scaling.y);
		}
	}
}
