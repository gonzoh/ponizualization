﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PonyRig : DataBus {

	Image targetImage;

	public Sprite[] avatarSprites;

	public float gain = 1f;

	float smoothedLevel = 0f;

	void Awake()
	{
		targetImage = GetComponent<Image>();
	}

	float lastChange = 0f;
	int previousIndex = 0;

	public override void Push(float[] data)
	{
		var level = data[0];

		if (level > smoothedLevel)
		{
			smoothedLevel = Mathf.Lerp(smoothedLevel, level, Time.deltaTime * 100f);
		} else {
			smoothedLevel = Mathf.Lerp(smoothedLevel, 0f, Time.deltaTime* 10f);
		}

		var index = Mathf.RoundToInt(smoothedLevel * gain * avatarSprites.Length);

		if (index > avatarSprites.Length - 1)
		{
			index = avatarSprites.Length - 1;
		}

		if (previousIndex != index)
		{
			if (Time.realtimeSinceStartup - lastChange < 0.1f)
			{
				index = previousIndex;
			} else {
				lastChange = Time.realtimeSinceStartup;
			}
		}

		previousIndex = index;
		targetImage.sprite = avatarSprites[index];
	}
}
