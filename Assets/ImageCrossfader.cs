﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ImageCrossfader : MonoBehaviour {

	public RawImage target;

	void Start()
	{
		StartCoroutine(FadeCoroutine());
	}

	IEnumerator FadeCoroutine()
	{
		while(true)
		{
			target.CrossFadeAlpha(0f,1f,true);
			yield return new WaitForSeconds(Random.Range(20f,30f));
			target.CrossFadeAlpha(1f,1f,true);
			yield return new WaitForSeconds(Random.Range(20f,30f));
		}
	}
}
