﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class NowPlaying : MonoBehaviour {

	public string url;

	Text label;

	void Start()
	{
		label = GetComponent<Text>();
		StartCoroutine(RefreshTitleCoroutine());
	}

	Dictionary<string,string> htmlEntities = new Dictionary<string, string> {
		{"&amp;","&"},
		{"&quot;","\""},
		{"&apos;","'"},
		{"&lt;","<"},
		{"&gt;",">"},
	};

	string SanitizeHTML(string html)
	{
		foreach (var kv in htmlEntities)
		{
			html = html.Replace(kv.Key,kv.Value);
		}
		return html;
	}

	IEnumerator RefreshTitleCoroutine()
	{
		var www = new WWW(url);

		yield return www;

		var title = SanitizeHTML(System.Text.Encoding.UTF8.GetString(www.bytes));
		label.text = "<b>"+title+"</b>";

		yield return new WaitForSeconds(10f);
		StartCoroutine(RefreshTitleCoroutine());
	}
}
