﻿using UnityEngine;
using System.Collections;

public abstract class DataBus : MonoBehaviour {

	public virtual void SetupWithWidth(int width)
	{
		ForwardWidthToAllReceivers(width);
	}

	public virtual void Push(float[] data)
	{
		ForwardToAllReceivers(data); 
	}

	public DataBus[] outBus;
	public void ForwardToAllReceivers(float[] data)
	{
		foreach (var db in outBus)
		{
			db.Push(data);
		}
	}

	public void ForwardWidthToAllReceivers(int width)
	{
		foreach (var db in outBus)
		{
			db.SetupWithWidth(width);
		}
	}
}
